import scala.io.Source
import scala.collection.JavaConversions._

import java.io.{File, PrintWriter, StringReader}

import tagger.Tagger
import usenet._

class SubtitleTagger(inputFolderPath: String, outputFolderPath: String, modelPath: String) {
  val t = new Tagger(modelPath)
  def tagFiles() = {
    val files = new File(inputFolderPath).listFiles.map(_.getName)
    files.foreach(tagFile)
  }

  def tagFile(fname: String) = {
    val inputFile = inputFolderPath + fname
    val outputFile = outputFolderPath + fname
    println(inputFile + " ===> " + outputFile)

    try {
      val fstr = t.tagFile(inputFile).foldLeft("")((a, b) => a + "\n-------\n" + b.foldLeft("")((g, h) => g + "\n" + h))
    val output = new PrintWriter(new File(outputFile))
    output.println(fstr)
    output.flush
    output.close
  } catch {
    case _: Throwable => println("Error! Input file: " + inputFile)
  }

}
}

object Process extends App {

  println("Scala application tagging and lemmatizing files in folder.")
  val defaultmodel = "/home/pm/tools/stanford-postagger-full-2013-04-04/models/wsj-0-18-left3words-distsim.tagger"
  if (args.length == 1 && "demo" == args(0))
    {
    println("This is demo")
    val tagger = new Tagger(defaultmodel)
    tagger.tagString("This is my demo sentence") foreach println

  }
  else if (args.length == 1 && "usenet" == args(0)) {
    println("Test usenet parser")
    val tagger = new Tagger(defaultmodel)
    val input = "/home/pm/tmp/head.usenet"
    val reader = usenetReader(input)
    reader take 2 map { e => (e._1, tagger.tagString(e._2)) } foreach { e => println(e._1); e._2 foreach { a => a foreach println}  }
  }
  else if (args.length == 3) { 
    val inputdir = args(0)
    val outputdir = args(1)
    val modelfile = args(2)
    val subParser = new SubtitleTagger(inputdir, outputdir, modelfile)
    subParser.tagFiles
  }
  else println ("Usage: <app> inputdir outputdir modelfile")
}
