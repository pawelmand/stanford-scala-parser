package usenet

import scala.io.Source

object usenetReader {
  val postEnd = "---END.OF.DOCUMENT---".r

  import java.nio.charset.CodingErrorAction
  import scala.io.Codec

  implicit val codec = Codec("UTF-8")
  codec.onMalformedInput(CodingErrorAction.REPLACE)
  codec.onUnmappableCharacter(CodingErrorAction.REPLACE)


  def checkEnd(a: String) = postEnd findFirstIn a match {
    case Some(s) => false
    case None => true
  }

  def apply(inputFile: String): Iterator[(Int, String)] =
  {
    val input = Source.fromFile(inputFile).getLines
    var counter = 0
    new Iterator[(Int, String)] {
      def hasNext = input.hasNext
      def next = {
        val build = new StringBuilder()
        input takeWhile checkEnd foreach { e => build.append(e).append('\n') }
        counter += 1
        (counter, build.toString)
      }
    }
  }
}


