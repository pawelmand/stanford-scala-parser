package tagger

import scala.io.Source
import scala.collection.JavaConversions._

import java.io.{File, PrintWriter, StringReader}

import edu.stanford.nlp.ling.{Sentence, TaggedWord, HasWord, CoreLabel}
import edu.stanford.nlp.process.{CoreLabelTokenFactory, DocumentPreprocessor, PTBTokenizer, Morphology}
import edu.stanford.nlp.objectbank.TokenizerFactory
import edu.stanford.nlp.tagger.maxent.MaxentTagger


class Tagger(modelPath: String) {
  val tagger = new MaxentTagger(modelPath)
  val ptbTokenizerFactory = PTBTokenizer.factory(new CoreLabelTokenFactory(), "untokenizable=noneKeep,americanize=False")
  val lemmatizer = new Morphology()

  def tagFile(filePath: String) = {
    val f = Source.fromFile(filePath).bufferedReader()
    val docPreproc = new DocumentPreprocessor(f)
    docPreproc.setTokenizerFactory(ptbTokenizerFactory)
    docPreproc.map(e => parseSentence(e))
  }

  def tagString(s: String) = {
    val r = new StringReader(s)
    val docPreproc = new DocumentPreprocessor(r)
    docPreproc.setTokenizerFactory(ptbTokenizerFactory)
    docPreproc.map(e => parseSentence(e))
  }


  def parseSentence(s: Seq[HasWord]) = {
    val taggedSentence = tagger.tagSentence(s)
    taggedSentence.map(e => lemmatize(e.word, e.tag))
  }

  def lemmatize(w: String, t: String): String = (w + "\t" +  t  + "\t" + lemmatizer.lemma(w, t))
}

