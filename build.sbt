import AssemblyKeys._ // put this at the top of the file

assemblySettings

name := "tag-stanford"

version := "0.2"

scalaVersion := "2.10.0"

libraryDependencies ++= Seq("org.scalaforge" % "scalax" % "0.1")

